drop table if exists Money;

create table Money(
	code varchar(3) primary key,
	exchangeRate double
)engine = InnoDb;

insert into Money (code, exchangeRate)
values
       ('EUR', 1),
       ('GBP', 1.17),
       ('USD', 0.90),
       ('MXN', 0.047);