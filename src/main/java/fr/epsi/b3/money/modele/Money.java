package fr.epsi.b3.money.modele;

import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Money extends RepresentationModel<Money> {

	/**
	 * le code ISO-4217
	 */
	@Id
	private String code;
	private BigDecimal exchangeRate;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
}
