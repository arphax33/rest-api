package fr.epsi.b3.money.controleur;

import fr.epsi.b3.money.modele.Money;
import fr.epsi.b3.money.service.MoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/money", produces = "application/hal+json", consumes = "application/json")
public class MoneyController {
    @Autowired
    private MoneyService moneyService;

    private static final String EURO_ISO_CODE = "EUR";

    @GetMapping(path = "/{code}")
    public ResponseEntity<Money> getById(@PathVariable String code, UriComponentsBuilder uriBuilder) {
        Money money = moneyService.getByCode(code);

        List<Money> nextAndPrev = moneyService.getNextAndPrev(money);
        Money next = nextAndPrev.get(0);
        Money prev = nextAndPrev.get(1);

        money.add(new Link(uriBuilder.path("/api/money/{code}").buildAndExpand(money.getCode()).toUriString()));
        money.add(new Link(uriBuilder.path("").buildAndExpand(next.getCode()).toUriString(), "next"));
        money.add(new Link(uriBuilder.path("").buildAndExpand(prev.getCode()).toUriString(), "prev"));
        return ResponseEntity.ok(money);
    }

    @PostMapping(path = "/", consumes = "application/json")
    public ResponseEntity<Money> create(@RequestBody Money money, UriComponentsBuilder uriBuilder) {
        moneyService.createOrUpdate(money);

        URI uri = uriBuilder.path("/api/money/{code}").buildAndExpand(money.getCode()).toUri();

        List<Money> nextAndPrev = moneyService.getNextAndPrev(money);
        Money next = nextAndPrev.get(0);
        Money prev = nextAndPrev.get(1);

        money.add(new Link(uri.toString()));
        money.add(new Link(uriBuilder.path("").buildAndExpand(next.getCode()).toUriString(), "next"));
        money.add(new Link(uriBuilder.path("").buildAndExpand(prev.getCode()).toUriString(), "prev"));
        return ResponseEntity.created(uri).body(money);
    }

    @DeleteMapping(path = "/{code}")
    public ResponseEntity<Money> delete(@RequestBody Money money) {
        moneyService.delete(money);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "/EUR/convert/{code}")
    public ResponseEntity<BigDecimal> convertToEuro(@PathVariable String code,
                                                    @RequestBody BigDecimal bdValue) {
        Money moneyOut = moneyService.getByCode(code);
        return ResponseEntity.ok(bdValue.multiply(moneyOut.getExchangeRate()));
    }
}
