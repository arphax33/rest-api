package fr.epsi.b3.money.service;

import fr.epsi.b3.money.dao.MoneyDao;
import fr.epsi.b3.money.modele.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Service
public class MoneyService {
    @Autowired
    private MoneyDao moneyDao;

    @Transactional(readOnly = true)
    public List<Money> getAll() {
        return moneyDao.getAll();
    }

    @Transactional(readOnly = true)
    public Money getByCode(@PathVariable String code) {
        return moneyDao.getByCode(code);
    }

    @Transactional
    public void createOrUpdate(Money money) {
        moneyDao.createOrUpdate(money);
    }

    @Transactional
    public void delete(Money money) {
        moneyDao.deleteByCode(money.getCode());
    }

    public List<Money> getNextAndPrev(Money searchMoney) {
        List<Money> allMoney = moneyDao.getAll();
        List<Money> returnedMoney = new ArrayList<>();
        int i = 0;

        for (Money money : allMoney) {
            i++;
            if (money.equals(searchMoney)) {
                if (i-1 < 0) { returnedMoney.add(allMoney.get(allMoney.size())); }
                else { returnedMoney.add(allMoney.get(i-1)); }

                if (i+1 > allMoney.size()-1) { returnedMoney.add(allMoney.get(0)); }
                else { returnedMoney.add(allMoney.get(i+1)); }

                break;
            }
        }

        return returnedMoney;
    }
}
