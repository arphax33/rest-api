package fr.epsi.b3.money.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.epsi.b3.money.modele.Money;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MoneyDao {
	
	@PersistenceContext
	private EntityManager em;


	public List<Money> getAll() {
		return em.createQuery("SELECT m FROM Money m", Money.class).getResultList();
	}

	/**
	 * Ajoute la monnaie dans la base de données. Si
	 * la monnaie existe déjà, cette méthode met à jour
	 * les informations de la monnaie (si nécessaire)
	 * 
	 * @param money
	 */
	public void createOrUpdate(Money money) {
		em.merge(money);
	}
	
	/**
	 * @param code
	 * @return Retourne la monnaie correspondant au code ou null si elle n'existe pas.
	 */
	public Money getByCode(String code) {
		return em.find(Money.class, code);
	}

	/**
	 * Supprime de la base de données une monnaie à partir de son code.
	 * Cette méthode est sans effet si la monnaie n'existe pas.
	 * @param code
	 */
	public void deleteByCode(String code) {
		em.createQuery("delete from Money m where m.code = :code")
		  .setParameter("code", code)
		  .executeUpdate();
	}
}
